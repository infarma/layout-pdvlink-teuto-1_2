package layout_pdvlink_teuto_1_2

import (
	"bitbucket.org/infarma/layout-pdvlink-teuto-1_2/remessaDePedidos"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (remessaDePedidos.ArquivoDePedido	, error) {
	return remessaDePedidos.GetStruct(fileHandle)
}
