package layoutDaNotaFiscal

import (
	"bufio"
	"os"
	"strconv"
)

type ArquivoDeNotaFiscal struct {
	HeaderDoArquivo       HeaderDoArquivo       `json:"HeaderDoArquivo"`
	CabecalhoDaNotaFiscal CabecalhoDaNotaFiscal `json:"CabecalhoDaNotaFiscal"`
	ItensDaNotaFiscal     []ItensDaNotaFiscal   `json:"ItensDaNotaFiscal"`
}

func GetStruct(fileHandle *os.File) (ArquivoDeNotaFiscal, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeNotaFiscal{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "00":
			err := arquivo.HeaderDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "05":
			err := arquivo.CabecalhoDaNotaFiscal.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "06":
			var registroTemp ItensDaNotaFiscal
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.ItensDaNotaFiscal = append(arquivo.ItensDaNotaFiscal, registroTemp)
			return arquivo, err
		}
	}
	return arquivo, err
}

func retornaComoInt64(valor string) int64 {
	vl, _ := strconv.ParseInt(valor, 10, 10)
	return vl
}

func retornaComoFloat64(value string, decimalPlaces int) float64 {
	some := value[:len(value)-decimalPlaces] + "." + value[len(value)-decimalPlaces:]
	result, _ := strconv.ParseFloat(some, 10)
	return result
}
