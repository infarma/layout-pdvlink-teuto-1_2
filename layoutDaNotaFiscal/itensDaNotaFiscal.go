package layoutDaNotaFiscal

import (
	"errors"
	"strings"
)

type ItensDaNotaFiscal struct {
	TipoDoRegistro               string  `json:"TipoDoRegistro"`
	CnpjDoCliente                string  `json:"CnpjDoCliente"`
	NumeroDaNotaFiscal           string  `json:"NumeroDaNotaFiscal"`
	SerieDaNotaFiscal            string  `json:"SerieDaNotaFiscal"`
	NumeroDoPedido               string  `json:"NumeroDoPedido"`
	NumeroDoPedidoDoDistribuidor string  `json:"NumeroDoPedidoDoDistribuidor"`
	SequenciaDoItem              string  `json:"SequenciaDoItem"`
	CodigoDoProduto              string  `json:"CodigoDoProduto"`
	QuantidadeFaturada           int64   `json:"QuantidadeFaturada"`
	ValorUnitarioDoItem          float64 `json:"ValorUnitarioDoItem"`
	ValorTotalDoItem             float64 `json:"ValorTotalDoItem"`
	BaseSt                       float64 `json:"BaseSt"`
	ValorSt                      float64 `json:"ValorSt"`
	BaseIcms                     float64 `json:"BaseIcms"`
	ValorIcms                    float64 `json:"ValorIcms"`
	ValorSuframa                 float64 `json:"ValorSuframa"`
	LoteDoMedicamento            string  `json:"LoteDoMedicamento"`
	CHaveDeAcesso                string  `json:"CHaveDeAcesso"`
}

func (i *ItensDaNotaFiscal) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 16 {
		i.TipoDoRegistro = items[0]
		i.CnpjDoCliente = items[1]
		i.NumeroDaNotaFiscal = items[2]
		i.SerieDaNotaFiscal = items[3]
		i.NumeroDoPedido = items[4]
		i.NumeroDoPedidoDoDistribuidor = items[5]
		i.SequenciaDoItem = items[6]
		i.CodigoDoProduto = items[7]
		i.QuantidadeFaturada = retornaComoInt64(items[8])
		i.ValorUnitarioDoItem = retornaComoFloat64(items[9], 4)
		i.ValorTotalDoItem = retornaComoFloat64(items[10], 4)
		i.BaseSt = retornaComoFloat64(items[11], 4)
		i.ValorSt = retornaComoFloat64(items[12], 4)
		i.BaseIcms = retornaComoFloat64(items[13], 4)
		i.ValorIcms = retornaComoFloat64(items[14], 4)
		i.ValorSuframa = retornaComoFloat64(items[15], 4)
		i.LoteDoMedicamento = items[16]
		i.CHaveDeAcesso = items[17]
	} else {
		return errors.New("Erro")
	}

	return nil
}
