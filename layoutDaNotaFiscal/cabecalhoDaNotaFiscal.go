package layoutDaNotaFiscal

import (
	"errors"
	"strings"
)

type CabecalhoDaNotaFiscal struct {
	TipoDoRegistro               string 	`json:"TipoDoRegistro"`
	CnpjDoCliente                string 	`json:"CnpjDoCliente"`
	NumeroDaNotaFiscal           string 	`json:"NumeroDaNotaFiscal"`
	SerieDaNotaFiscal            string 	`json:"SerieDaNotaFiscal"`
	NumeroDoPedido               string 	`json:"NumeroDoPedido"`
	NumeroDoPedidoDoDistribuidor string 	`json:"NumeroDoPedidoDoDistribuidor"`
	DataDeFaturamento            string 	`json:"DataDeFaturamento"`
	HoraDeFaturamento            string 	`json:"HoraDeFaturamento"`
	TipoDeNotaFiscal             string 	`json:"TipoDeNotaFiscal"`
	TotalFaturado                float64	`json:"TotalFaturado"`
	ValorSt                      float64	`json:"ValorSt"`
	ValorIcms                    float64	`json:"ValorIcms"`
	BaseSt                       float64	`json:"BaseSt"`
	BaseIcms                     float64	`json:"BaseIcms"`
	ValorSuframa                 float64	`json:"ValorSuframa"`
}

func (c *CabecalhoDaNotaFiscal) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 8 {
		c.TipoDoRegistro = items[0]
		c.CnpjDoCliente = items[1]
		c.NumeroDaNotaFiscal = items[2]
		c.SerieDaNotaFiscal = items[3]
		c.NumeroDoPedido = items[4]
		c.NumeroDoPedidoDoDistribuidor = items[5]
		c.DataDeFaturamento = items[6]
		c.HoraDeFaturamento = items[7]
		c.TipoDeNotaFiscal = items[8]
		c.TotalFaturado = retornaComoFloat64(items[9], 4)
		c.ValorSt = retornaComoFloat64(items[10], 4)
		c.ValorIcms = retornaComoFloat64(items[11], 4)
		c.BaseSt = retornaComoFloat64(items[12], 4)
		c.BaseIcms = retornaComoFloat64(items[13], 4)
		c.ValorSuframa = retornaComoFloat64(items[14], 4)
	} else {
		return errors.New("Erro")
	}

	return nil
}