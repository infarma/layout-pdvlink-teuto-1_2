package retornoDePedidos

import (
	"errors"
	"strings"
)

type CabecalhoDoPedido struct {
	TipoDoRegistro                 string  `json:"TipoDoRegistro"`
	CnpjDoCliente                  string  `json:"CnpjDoCliente"`
	NumeroDoPedido                 int64   `json:"NumeroDoPedido"`
	NumeroDoPedidoDoDistribuidor   int64   `json:"NumeroDoPedidoDoDistribuidor"`
	DataDeGeracaoDeRetorno         string  `json:"DataDeGeracaoDeRetorno"`
	HoraDeGeracaoDeRetorno         string  `json:"HoraDeGeracaoDeRetorno"`
	ValorAtendido                  float64 `json:"ValorAtendido"`
	PosicaoDoPedido                string  `json:"PosicaoDoPedido"`
	CodigoDoMotivoDeNaoAtendimento string  `json:"CodigoDoMotivoDeNaoAtendimento"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 9 {
		c.TipoDoRegistro = items[0]
		c.CnpjDoCliente = items[1]
		c.NumeroDoPedido = retornaComoInt64(items[2])
		c.NumeroDoPedidoDoDistribuidor = retornaComoInt64(items[3])
		c.DataDeGeracaoDeRetorno = items[4]
		c.HoraDeGeracaoDeRetorno = items[5]
		c.ValorAtendido = retornaComoFloat64(items[6], 4)
		c.PosicaoDoPedido = items[7]
		c.CodigoDoMotivoDeNaoAtendimento = items[8]
	} else {
		return errors.New("Erro")
	}

	return nil
}
