package remessaDePedidos

import (
	"errors"
	"strings"
)

type ItemDetalheDoPedido struct {
	TipoDoRegistro      string  `json:"TipoDoRegistro"`
	CnpjDoCliente       string  `json:"CnpjDoCliente"`
	NumeroDoPedido      int64   `json:"NumeroDoPedido"`
	SequenciaDoItem     int64   `json:"SequenciaDoItem"`
	CodigoDoProduto     string  `json:"CodigoDoProduto"`
	Quantidade          int64   `json:"Quantidade"`
	Desconto            float64 `json:"Desconto"`
	ValorUnitarioDoItem float64 `json:"ValorUnitarioDoItem"`
	ValorTotalDoItem    float64 `json:"ValorTotalDoItem"`
	TipoDoItem          string  `json:"TipoDoItem"`
}

func (i *ItemDetalheDoPedido) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 9 {
		i.TipoDoRegistro = items[0]
		i.CnpjDoCliente = items[1]
		i.NumeroDoPedido = retornaComoInt64(items[2])
		i.SequenciaDoItem = retornaComoInt64(items[3])
		i.CodigoDoProduto = items[4]
		i.Quantidade = retornaComoInt64(items[5])
		i.Desconto = retornaComoFloat64(items[6], 2)
		i.ValorUnitarioDoItem = retornaComoFloat64(items[7], 4)
		i.ValorTotalDoItem = retornaComoFloat64(items[8], 4)
		i.TipoDoItem = items[9]
	} else {
		return errors.New("Erro")
	}

	return nil
}
