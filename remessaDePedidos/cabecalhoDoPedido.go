package remessaDePedidos

import (
	"errors"
	"strings"
)

type CabecalhoDoPedido struct {
	TipoDeRegistro          string  `json:"TipoDeRegistro"`
	CnpjDoCliente           string  `json:"CnpjDoCliente"`
	NumeroDoPedido          int64   `json:"NumeroDoPedido"`
	DataDoPedido            string  `json:"DataDoPedido"`
	PrazoDePagamento        string  `json:"PrazoDePagamento"`
	NumeroDoPedidoDoCliente int64   `json:"NumeroDoPedidoDoCliente"`
	TotalDoPedido           float64 `json:"TotalDoPedido"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	items := strings.Split(fileContents, ";")

	if len(items) >= 6 {
		c.TipoDeRegistro = items[0]
		c.CnpjDoCliente = items[1]
		c.NumeroDoPedido = retornaComoInt64(items[2])
		c.DataDoPedido = items[3]
		c.PrazoDePagamento = items[4]
		c.NumeroDoPedidoDoCliente = retornaComoInt64(items[5])
		c.TotalDoPedido = retornaComoFloat64(items[6], 4)
	} else {
		return errors.New("Erro")
	}

	return nil
}
