package layoutDoEstoqueDoDistribuidor

type ArquivoDeEstoque struct {
	TipoRegistro      string	`json:"TipoRegistro"`
	CnpjDistribuidor  string	`json:"CnpjDistribuidor"`
	DataProcessamento string	`json:"DataProcessamento"`
	HoraProcessamento string	`json:"HoraProcessamento"`
}